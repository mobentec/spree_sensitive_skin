module Spree
  HomeController.class_eval do
    def sensitive
      @products = Product.joins(:variants_including_master).where('spree_variants.sensitive_skin2 is not null').uniq
    end
    end
end